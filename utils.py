#!/usr/bin/python
# coding:utf-8


# 打开目录
# def start_file(file_name):
#     import os
#     import subprocess
#     print(file_name)
#     try:
#         os.startfile(file_name)
#     except:
#         subprocess.Popen(['xdg-open', file_name])

# 打开目录
def start_file(file_name):
    import os
    import sys
    import subprocess
    if sys.platform == 'win32':
        os.startfile(file_name)
    elif sys.platform == 'darwin':
        subprocess.call(['open', file_name])
    else:
        subprocess.call(['xdg-open', file_name])
    # opener = 'open' if sys.platform == 'darwin' else 'xdg-open'
    # subprocess.call([opener, file_name])


if __name__ == "__main__":
    dir = '/home/panda/Videos/懒饭'
    start_file(dir)
