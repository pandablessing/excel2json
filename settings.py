#!/usr/bin/python
# coding:utf-8

# excel目录
EXCEL_DIR = './excel'
# json目录
JSON_DIR = './json'
# 导出文件名称
NAME_EXPORT = 'Configs'

LANGUAGE = 'zh_cn'
STR_TO_JSON_TYPE = 'Condition'
