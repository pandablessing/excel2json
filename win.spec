# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['mainUI.py','main.py','settings.py','language.py','utils.py'],
             pathex=['i:\\work\\python\\excel2json'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='转表工具v2023.6.16',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          icon='i:\\work\\python\\excel2json\\fav.ico',
          console=False )
